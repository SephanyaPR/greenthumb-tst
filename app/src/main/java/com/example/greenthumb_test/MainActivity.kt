package com.example.greenthumb_test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(isNetworkConnected()){
            Toast.makeText(this,"Network is Connected",Toast.LENGTH_LONG).show()
            showLoading()
            textviewMainActivityTextView.setText("Network is Available")

        }
        else if (!isNetworkConnected()){
            Toast.makeText(this,"Network is not Connected",Toast.LENGTH_LONG).show()
            hideLoading()
            textviewMainActivityTextView.setText("Network is Not Available")
        }
    }
}